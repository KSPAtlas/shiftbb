#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <sys/stat.h>

bool cells[8192];
bool incond;
int ptr, retptr = 0;

unsigned char charbuf, testcond;

void initbytewith(char val) {
	for(int i = ptr; i <= ptr+8; i++) {
		cells[i] = (val & (1 << i)) >> i;
	}
}

char* readfile(char* fn) {
	FILE *fp;
	fp = fopen("main.sbb", "r");
	struct stat sb;
	stat(fn, &sb);
	char* code = malloc(sb.st_size);
	fread(code, 1, sb.st_size, fp);
	return code;
}

// Required for the get char function, to ensure that the bits are in the correct order
unsigned int reversebits(unsigned char num) {
	unsigned int NO_OF_BITS = sizeof(num) * 8;
	unsigned char reverse_num = 0;
	for (unsigned int i = 0; i < NO_OF_BITS; i++) {
		if((num & (1 << i)))
			reverse_num |= 1 << ((NO_OF_BITS - 1) - i);  
	}
	return reverse_num;
}

int main(int argc, char** argv) {
	if (argc < 2) return 1;
	char* code = readfile(argv[1]);
	for(;;) {
		for(int i = 0;;i++) {
			switch(code[i]) {
				case '1': 
					initbytewith(0x00);
					break;
				case '2':
					initbytewith(0xff);
					break;
				case '.':
					ptr++;
					break;
				case ',':
					ptr--;
					break;
				case '_':
					for(int j = ptr; j < ptr+8; j++) charbuf = (charbuf << 1) | cells[j];
					if(charbuf == 0x00) {
						for(int k = ptr-8; k < ptr; k++) charbuf = (charbuf << 1) | cells[k];
						printf("%c", charbuf);
					} else if(charbuf == 0xff) {
						for(int k = ptr-8; k < ptr; k++) charbuf = (charbuf << 1) | cells[k];
						printf("0x%02x", charbuf);
					}
					break;

				case '/':
					printf("\n");
					free(code);
					return 0;
				
				case '@':
					incond = true;
					for(int j = ptr; j < ptr+8; j++) charbuf = (charbuf << 1) | cells[j];
					testcond = charbuf;
					retptr = i;
					break;

				case '=':
					if (!incond) break;
					for(int j = ptr; j < ptr+8; j++) charbuf = (charbuf << 1) | cells[j];
					if (charbuf == testcond) i = retptr;
					break;

				case '?':
					initbytewith(reversebits(getchar()));
					break;
				default:
					break;
			}
		}
	}
}
					
