# shiftbB

ShiftbB is a cell-based esolang. 

## Compilation
Compile with `gcc main.c -Wall -Wextra -Werror -o sbb`.

## Running
Simply run `./sbb <name-of-file>`
